﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowSkills : MonoBehaviour {
	private int damage;
	private int speed;
	private int power;

	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		damage = PlayerPrefs.GetInt ("BowDamage");
		speed = PlayerPrefs.GetInt ("BowSpeed");
		power = PlayerPrefs.GetInt ("BowPower");
	}

	public int GetDamage(){
		return damage;
	}

	public int GetSpeed(){
		return speed;
	}

	public int GetPower(){
		return power;
	}

	public void AddDamage(int n){
		damage += n;
		PlayerPrefs.SetInt ("BowDamage", damage);
	}

	public void AddSpeed(int n){
		speed += n;
		PlayerPrefs.SetInt ("BowSpeed", speed);
	}

	public void AddPower(int n){
		power += n;
		PlayerPrefs.SetInt ("BowPower", power);
	}
}