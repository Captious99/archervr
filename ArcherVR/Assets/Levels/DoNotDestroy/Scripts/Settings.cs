﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {
	private int sensetivityForShoot;

	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		sensetivityForShoot = PlayerPrefs.GetInt ("SensetivityForShoot");
	}

	public void ChangeSensShoot(int n){
		sensetivityForShoot += n;
		PlayerPrefs.SetInt ("SensetivityForShoot", sensetivityForShoot);
	}
		
	public int GetSensShoot(){
		return sensetivityForShoot;
	}
}