﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour {
	private int myGold;

	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		myGold = PlayerPrefs.GetInt ("Gold");
	}

	public void AddGold(int count){
		myGold += count;
		PlayerPrefs.SetInt ("Gold", myGold);	
	}

	public void MinusGold(int count){
		myGold -= count;
		PlayerPrefs.SetInt ("Gold", myGold);
	}

	public int GetGold(){
		return myGold;
	}
}