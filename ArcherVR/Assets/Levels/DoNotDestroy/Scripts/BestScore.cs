﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestScore : MonoBehaviour {
	public enum GameMode {Training, Fruit, Target};

	private int bestScoreTraining;
	private int bestScoreFruit;
	private int bestScoreTarget;

	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		bestScoreTraining = PlayerPrefs.GetInt ("BestTraining");
		bestScoreFruit = PlayerPrefs.GetInt ("BestFruit");
		bestScoreTarget = PlayerPrefs.GetInt ("BestTarget");
	}

	public bool TakeScore(int score, GameMode mode){
		if (mode == GameMode.Training) {
			if (score > bestScoreTraining) {
				bestScoreTraining = score;
				PlayerPrefs.SetInt ("BestTraining", bestScoreTraining);
				return true;
			}
		} else if (mode == GameMode.Fruit){
			if (score > bestScoreFruit) {
				bestScoreFruit = score;
				PlayerPrefs.SetInt ("BestFruit", bestScoreFruit);
				return true;
			}
		} else if (mode == GameMode.Target){
			if (score > bestScoreTarget) {
				bestScoreTarget = score;
				PlayerPrefs.SetInt ("BestTarget", bestScoreTarget);
				return true;
			}
		}
		return false;
	}

	public int GetBestTraining(){
		return bestScoreTraining;
	}

	public int GetBestFruit(){
		return bestScoreFruit;
	}

	public int GetBestTarget(){
		return bestScoreTarget;
	}
}