﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour {
	public GameObject myCamera;
	public GameObject menyButton;
	public GameObject minusSens;
	public GameObject addSens;
	public GameObject resumeButton;
	public Image aimLoad;

	private float buttonTimer;
	private bool activePause;
	public GameObject buttonForActive;



	public GameObject pauseMeny;
	public GameObject texts;
	public GameObject archer;
	public GameObject generator;

	void Update () {
		if (activePause) {
			GoMeny ();
		} else {
			ActivePauseMeny ();
		}
	}

	private void Resume(){
		pauseMeny.SetActive (false);
		buttonForActive.SetActive (true);
		activePause = false;
		texts.SetActive (true);
		generator.SetActive (true);
		archer.GetComponent<ShakeForStartShoot> ().enabled = true;
		archer.GetComponent<TimeForEnd> ().enabled = true;
	}

	public void PauseActive(){
		pauseMeny.SetActive (true);
		buttonForActive.SetActive (false);
		activePause = true;
		texts.SetActive (false);
		generator.SetActive (false);
		archer.GetComponent<ShakeForStartShoot> ().enabled = false;
		archer.GetComponent<TimeForEnd> ().enabled = false;
	}

	private void ActivePauseMeny(){
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if (hit.transform == buttonForActive.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if (buttonTimer >= 2.0f) {
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					SoundButton ();
					PauseActive ();
				}
			} else {
				buttonTimer = 0;
			}
		}
	}

	public void GoMeny(){
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if (hit.transform == menyButton.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if (buttonTimer >= 2.0f) {
					SoundButton ();
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					Application.LoadLevel (0);
				}
			} else if (hit.transform == resumeButton.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if (buttonTimer >= 2.0f) {
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					Resume ();
					SoundButton ();
				}
			} else if (hit.transform == addSens.transform) {
				if (GameObject.FindWithTag ("Settings").GetComponent<Settings> ().GetSensShoot () < 10) {
					buttonTimer += Time.deltaTime;
					aimLoad.fillAmount = buttonTimer / 2.0f;
					if (buttonTimer >= 2.0f) {
						aimLoad.fillAmount = 0;
						buttonTimer = 0;
						GameObject.FindWithTag ("Settings").GetComponent<Settings> ().ChangeSensShoot (1);
						SoundButton ();
					}
				}
			} else if (hit.transform == minusSens.transform) {
				if (GameObject.FindWithTag ("Settings").GetComponent<Settings> ().GetSensShoot () > 0) {
					buttonTimer += Time.deltaTime;
					aimLoad.fillAmount = buttonTimer / 2.0f;
					if (buttonTimer >= 2.0f) {
						aimLoad.fillAmount = 0;
						buttonTimer = 0;
						GameObject.FindWithTag ("Settings").GetComponent<Settings> ().ChangeSensShoot (-1);
						SoundButton ();
					}
				}
			} else {
				aimLoad.fillAmount = 0;
				buttonTimer = 0;
			}
		}
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}