﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResenterButton : MonoBehaviour {
	public GameObject myCamera;
	public GameObject forRotateCamera;
	public GameObject resButton;
	public Image aimLoad;
	private float buttonTimer;
	private bool doStartRot = false;

	void Update () {
		/*
		if (!doStartRot) {
			Reseneter ();
			Reseneter ();
			doStartRot = true;
		}
		*/
		ResButton ();
	}

	void LateUpdate(){
        /*
		if (!doStartRot) {
			Reseneter ();
			Reseneter ();
			doStartRot = true;
		}
        */
	}

	private void ResButton(){
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if (hit.transform == resButton.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if (buttonTimer >= 2.0f) {
					SoundButton ();
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					Reseneter ();
				}
			} else {
				buttonTimer = 0;
			}
		}
	}

	private void Reseneter(){
		forRotateCamera.transform.localRotation *= Quaternion.Euler(0, -myCamera.transform.rotation.y*150, 0);
		forRotateCamera.transform.localRotation *= Quaternion.Euler(0, -myCamera.transform.rotation.y*150, 0);
		forRotateCamera.transform.localRotation *= Quaternion.Euler(0, -myCamera.transform.rotation.y*150, 0);
		forRotateCamera.transform.localRotation *= Quaternion.Euler(0, -myCamera.transform.rotation.y*150, 0);
		forRotateCamera.transform.localRotation *= Quaternion.Euler(0, -myCamera.transform.rotation.y*150, 0);
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}