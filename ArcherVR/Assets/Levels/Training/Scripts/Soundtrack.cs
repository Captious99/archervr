﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundtrack : MonoBehaviour {
	public AudioClip audio1;
	public AudioClip audio2;
	public AudioClip audio3;

	void Start () {
		int r = Random.Range (0, 3);
		switch (r) {
		case 0: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio1;
			break;
		case 1: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio2;
			break;
		case 2: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio3;
			break;
		}
		this.gameObject.GetComponent<AudioSource> ().Play ();
	}
}
