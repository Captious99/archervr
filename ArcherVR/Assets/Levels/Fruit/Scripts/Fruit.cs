﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour {
	public int health;
	public Scores scores;
	public Ammo ammo;
	public TimeForEnd timefe;
	public int scoresForKill;
	public int ammoForKill;
	public int timeForKill;
	public ThrowFruit thrower;
	private float timerDead;
	public bool bigFruit;

	void Start(){
		int damage = 1+GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ().GetDamage ();
		if (bigFruit) {
			health = damage + Random.Range (1, 20);
		} else {
			health = damage;
		}
	}

	void Update () {
		TimeLiveOff ();
	}

	public void SetThrower(ThrowFruit thr){
		thrower = thr;
	}

	public void SetArcher(GameObject archer){
		scores = archer.GetComponent<Scores> ();
		ammo = archer.GetComponent<Ammo> ();
		timefe = archer.GetComponent<TimeForEnd> ();
	}

	private void TimeLiveOff(){
		timerDead += Time.deltaTime;
		if (timerDead >= 8.5f) {
			deadForGenerator ();
			Destroy (this.gameObject);
		}
	}

	public void TakeDamage(int damage){
		health -= damage;
		Dead ();
	}

	private void Dead(){
		if (health <= 0) {
			scores.currentScores += scoresForKill;
			scores.UpdateText ();
			ammo.currentAmmo += ammoForKill;
			ammo.UpdateText ();
			timefe.currentTime += timeForKill;
			deadForGenerator ();
			Destroy (this.gameObject);
		}
	}

	private void deadForGenerator(){
		thrower.currentTargets--;
	}
}
