﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFruit : MonoBehaviour {
	public GameObject[] spawnPoints = new GameObject[5];
	public Transform[] fruits = new Transform[5];
	public ThrowFruit thrower;
	public GameObject archer;

	public float maxTarget;
	public int currentTargets;

	public float timeSpawn;
	private float spawnTaimer;

	void Update () {
		SpawnFruit ();
	}
	
	private void SpawnFruit(){
		if (currentTargets < (int)maxTarget) {
			spawnTaimer += Time.deltaTime;
			if (spawnTaimer >= timeSpawn) {
				FruitThrow ();
				currentTargets++;
				spawnTaimer = 0;
				if (timeSpawn > 4.0f) {
					timeSpawn -= 0.1f;
				}
				if (maxTarget < 10.0f) {
					maxTarget += 0.2f;
				}
			}
		}
	}

	private void FruitThrow(){
		Transform FruitInstance = (Transform)Instantiate (FindFruit(), FindSpawnPoint().transform.position, FindSpawnPoint().transform.rotation); 
		FruitInstance.GetComponent<Rigidbody> ().AddForce (FindSpawnPoint().transform.forward * FindForceThrow()); 
	}

	private float FindForceThrow(){
		return Random.Range (4000, 4500);
	}

	private	GameObject FindSpawnPoint(){
		int r = Random.Range(0,spawnPoints.Length);
		return spawnPoints [r];
	}

	private Transform FindFruit(){
		int typeFriit = Random.Range(0,101);
		if (typeFriit < 85) {
			int r = Random.Range (1, fruits.Length);
			Transform fruit = fruits [r];
			fruit.gameObject.GetComponent<Fruit> ().SetThrower (thrower);
			fruit.gameObject.GetComponent<Fruit> ().SetArcher (archer);
			return fruit;
		} else if (typeFriit >= 85) {
			Transform fruit = fruits [0];
			fruit.gameObject.GetComponent<Fruit> ().SetThrower (thrower);
			fruit.gameObject.GetComponent<Fruit> ().SetArcher (archer);
			return fruit;
		} else {
			return null;
		}
	}
}