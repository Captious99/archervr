﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitAnim : MonoBehaviour {
	
	private Transform thisFruit;
	private int speedRotationX;
	private int speedRotationY;
	private int speedRotationZ;

	void Start () {
		thisFruit = this.transform;
		speedRotationX = Random.Range (50,70);
		speedRotationY = Random.Range (50,70);
		speedRotationZ = Random.Range (50,70);
	}

	void Update () {
		thisFruit.rotation *= Quaternion.Euler (Time.deltaTime*speedRotationX, Time.deltaTime*speedRotationY, Time.deltaTime*speedRotationZ);
	}
}
