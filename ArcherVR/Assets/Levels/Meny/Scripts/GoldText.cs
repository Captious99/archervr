﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldText : MonoBehaviour {

	public TextMesh goldText;

	void Update () {
		goldText.text = GameObject.FindWithTag ("Gold").GetComponent<Gold> ().GetGold().ToString();
	}
}