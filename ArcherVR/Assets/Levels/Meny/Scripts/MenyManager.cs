﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenyManager : MonoBehaviour {
	public enum Meny {MainMeny, ChooseModePlay, FruitMode, TrainingMode, TargetMode, EquipmentMeny, SettingsMeny, AboutTraining, 
		AboutTarget, AboutFruit, AboutEquipment};

	public GameObject mainMeny;
	public GameObject chooseModePlay;
	public GameObject fruitMode;
	public GameObject targetMode;
	public GameObject trainingMode;
	public GameObject equipmentMeny;
	public GameObject settingsMeny;
	public GameObject aboutTrainingMeny;
	public GameObject aboutTargetMeny;
	public GameObject aboutFruitMeny;
	public GameObject aboutEquipmentMeny;

	void Start(){
		mainMeny.SetActive (true);
		chooseModePlay.SetActive (false);
		fruitMode.SetActive (false);
		targetMode.SetActive (false);
		trainingMode.SetActive (false);
		equipmentMeny.SetActive (false);
		settingsMeny.SetActive (false);
		aboutTrainingMeny.SetActive (false);
		aboutTargetMeny.SetActive (false);
		aboutFruitMeny.SetActive (false);
		aboutEquipmentMeny.SetActive (false);
	}

	public void ChangeMeny(Meny needMeny){
		switch (needMeny) {
			case Meny.MainMeny:
				mainMeny.SetActive (true);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.ChooseModePlay:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (true);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.FruitMode:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (true);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.TrainingMode:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (true);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.TargetMode:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (true);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.EquipmentMeny:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (true);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.SettingsMeny:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (true);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.AboutTraining:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (true);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.AboutEquipment:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (true);
				break;
			case Meny.AboutFruit:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (false);
				aboutFruitMeny.SetActive (true);
				aboutEquipmentMeny.SetActive (false);
				break;
			case Meny.AboutTarget:
				mainMeny.SetActive (false);
				chooseModePlay.SetActive (false);
				fruitMode.SetActive (false);
				targetMode.SetActive (false);
				trainingMode.SetActive (false);
				equipmentMeny.SetActive (false);
				settingsMeny.SetActive (false);
				aboutTrainingMeny.SetActive (false);
				aboutTargetMeny.SetActive (true);
				aboutFruitMeny.SetActive (false);
				aboutEquipmentMeny.SetActive (false);
				break;
		}
	}
}