﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestScoreText : MonoBehaviour {
	public enum GameMode {Training, Fruit, Target};

	public GameMode thisMode;

	void Update () {
		if (thisMode == GameMode.Training) {
			this.gameObject.GetComponent<TextMesh>().text = "Best score: " + GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().GetBestTraining().ToString();
		} else if (thisMode == GameMode.Fruit) {
			this.gameObject.GetComponent<TextMesh>().text = "Best score: " + GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().GetBestFruit().ToString();
		} else if (thisMode == GameMode.Target) {
			this.gameObject.GetComponent<TextMesh>().text = "Best score: " + GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().GetBestTarget().ToString();
		}
	}
}