﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResetAim : MonoBehaviour {
	
	public Image aimLoad;
	private float amount;
	private bool first;

	void Start(){
		first = true;
	}

	void Update () {
		if (first) {
			amount = aimLoad.fillAmount;
			first = false;
		} else {
			if (amount >= aimLoad.fillAmount) {
				aimLoad.fillAmount = 0;
			}
			first = true;
		}
	}
}