﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonForBuyUpgrade : MonoBehaviour {
	public enum Characteristic{Damage, Speed, Power};

	public Characteristic charac;
	public TextMesh costText;
	public GameObject buttonForUpSkill;
	public GameObject myCamera;
	public Image aimLoad;

	private BowSkills bowSkills;
	private Gold gold;

	private bool visibleButton;
	private float buttonTimer;

	void Update () {
		bowSkills = GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ();
		gold = GameObject.FindWithTag ("Gold").GetComponent<Gold> ();

		UpdateTextCost ();
		VisibleButton ();
		if (visibleButton) {
			ButtonSkill ();
		}
	}

	private void ButtonSkill(){
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if (hit.transform == buttonForUpSkill.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if(buttonTimer >= 2.0f){
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					UpSkill ();
				}
			} else {
				buttonTimer = 0;
			}
		}
	}

	private void UpSkill(){
		if (charac == Characteristic.Damage) {
			gold.MinusGold(CostUpgrade(bowSkills.GetDamage()));
			bowSkills.AddDamage(1);
			SoundButton ();
		} else if (charac == Characteristic.Speed) {
			gold.MinusGold(CostUpgrade(bowSkills.GetSpeed()));
			bowSkills.AddSpeed(1);
			SoundButton ();
		} else if (charac == Characteristic.Power) {
			gold.MinusGold(CostUpgrade(bowSkills.GetPower()));
			bowSkills.AddPower(1);
			SoundButton ();
		}
	}

	private void VisibleButton(){
		if (charac == Characteristic.Damage) {
			if (bowSkills.GetDamage() < 10 && gold.GetGold() >= CostUpgrade (bowSkills.GetDamage())) {
				buttonForUpSkill.SetActive (true);
				visibleButton = true;
			} else {
				buttonForUpSkill.SetActive (false);
				visibleButton = false;
			}
		} else if (charac == Characteristic.Speed) {
			if (bowSkills.GetSpeed() < 10 && gold.GetGold() >= CostUpgrade (bowSkills.GetSpeed())) {
				buttonForUpSkill.SetActive (true);
				visibleButton = true;
			} else {
				buttonForUpSkill.SetActive (false);
				visibleButton = false;
			}
		} else if (charac == Characteristic.Power) {
			if (bowSkills.GetPower() < 10 && gold.GetGold() >= CostUpgrade (bowSkills.GetPower())) {
				buttonForUpSkill.SetActive (true);
				visibleButton = true;
			} else {
				buttonForUpSkill.SetActive (false);
				visibleButton = false;
			}
		}
	}

	private void UpdateTextCost(){
		if (charac == Characteristic.Damage) {
			if (bowSkills.GetDamage() == 10) {
				costText.text = "max";
			} else {
				costText.text = "Gold: " + CostUpgrade (bowSkills.GetDamage());
			}
		} else if (charac == Characteristic.Speed) {
			if (bowSkills.GetSpeed() == 10) {
				costText.text = "max";
			} else {
				costText.text = "Gold: " + CostUpgrade (bowSkills.GetSpeed());
			}
		} else if (charac == Characteristic.Power) {
			if (bowSkills.GetPower() == 10) {
				costText.text = "max";
			} else {
				costText.text = "Gold: " + CostUpgrade (bowSkills.GetPower());
			}
		}
	}

	private int CostUpgrade(int currentLvl){
		switch (currentLvl) {
			case 0: return 5;
			case 1: return 15;
			case 2: return 25;
			case 3: return 35;
			case 4: return 50;
			case 5: return 65;
			case 6: return 80;
			case 7: return 100;
			case 8: return 125;
			case 9: return 150;
			default: return 0;
		}
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}