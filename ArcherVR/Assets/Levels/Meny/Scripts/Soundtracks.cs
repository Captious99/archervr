﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundtracks : MonoBehaviour {
	
	public AudioClip audio1;
	public AudioClip audio2;
	public AudioClip audio3;
	public AudioClip audio4;

	void Start () {
		int r = Random.Range (0, 4);
		switch (r) {
		case 0: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio1;
			break;
		case 1: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio2;
			break;
		case 2: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio3;
			break;
		case 3: 
			this.gameObject.GetComponent<AudioSource> ().clip = audio4;
			break;
		}
		this.gameObject.GetComponent<AudioSource> ().Play ();
	}
}
