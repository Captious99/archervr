﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonForStartLevel : MonoBehaviour {
	public int needLevel;
	public GameObject myCamera;
	public Image aimLoad;

	private float buttonTimer;

	void Update () {
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if (hit.transform == this.transform) {
				buttonTimer += Time.deltaTime;
				aimLoad.fillAmount = buttonTimer / 2.0f;
				if(buttonTimer >= 2.0f){
					SoundButton ();
					aimLoad.fillAmount = 0;
					buttonTimer = 0;
					StartLevel ();
				}
			} else {
				buttonTimer = 0;
			}
		}
	}

	private void StartLevel(){
		Application.LoadLevel (needLevel);
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}