﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowSkillText : MonoBehaviour {
	
	public enum Characteristic {Damage, Speed, Power};

	public Characteristic charac;

	void Update () {
		UpdateText ();
	}

	private void UpdateText(){
		if (charac == Characteristic.Damage) {
			this.gameObject.GetComponent<TextMesh>().text = GameObject.FindWithTag("BowSkills").GetComponent<BowSkills>().GetDamage().ToString() + "/10";
		} else if (charac == Characteristic.Power) {
			this.gameObject.GetComponent<TextMesh>().text = GameObject.FindWithTag("BowSkills").GetComponent<BowSkills>().GetPower().ToString() + "/10";
		} else if (charac == Characteristic.Speed) {
			this.gameObject.GetComponent<TextMesh>().text = GameObject.FindWithTag("BowSkills").GetComponent<BowSkills>().GetSpeed().ToString() + "/10";
		}
	}
}