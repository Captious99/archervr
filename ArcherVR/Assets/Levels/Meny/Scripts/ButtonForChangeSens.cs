﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonForChangeSens : MonoBehaviour {
	public enum TypeButton {Add, Minus};

	public TypeButton typeButton;
	public GameObject myCamera;
	public Image aimLoad;

	private float buttonTimer;

	void Update(){
		if (typeButton == TypeButton.Add) {
			if (GameObject.FindWithTag ("Settings").GetComponent<Settings> ().GetSensShoot() < 10) {
				RaycastHit hit;
				Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
				if (Physics.Raycast (ray, out hit)) {
					if (hit.transform == this.transform) {
						buttonTimer += Time.deltaTime;
						aimLoad.fillAmount = buttonTimer / 2.0f;
						if(buttonTimer >= 2.0f){
							aimLoad.fillAmount = 0;
							buttonTimer = 0;
							SoundButton ();
							ChangeSesetivity (1);
						}
					} else {
						buttonTimer = 0;
					}
				}
			}
		} else if (typeButton == TypeButton.Minus) {
			if (GameObject.FindWithTag ("Settings").GetComponent<Settings> ().GetSensShoot() > 0) {
				RaycastHit hit;
				Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
				if (Physics.Raycast (ray, out hit)) {
					if (hit.transform == this.transform) {
						buttonTimer += Time.deltaTime;
						aimLoad.fillAmount = buttonTimer / 2.0f;
						if(buttonTimer >= 2.0f){
							aimLoad.fillAmount = 0;
							buttonTimer = 0;
							SoundButton ();
							ChangeSesetivity (-1);
						}
					} else {
						buttonTimer = 0;
					}
				}
			}
		}
	}

	private void ChangeSesetivity(int count){
		GameObject.FindWithTag ("Settings").GetComponent<Settings> ().ChangeSensShoot(count);
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}