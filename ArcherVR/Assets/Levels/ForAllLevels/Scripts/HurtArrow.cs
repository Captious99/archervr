﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtArrow : MonoBehaviour {
	public float lifeTime = 3.0f;
	private float lifeTaimer;

	void Update () {
		lifeTaimer += Time.deltaTime;
		if (lifeTaimer >= lifeTime) {
			Destroy (this.gameObject);
		}
	}
}