﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scores : MonoBehaviour {
	public int currentScores;
	public TextMesh scoresText;

	void Start(){
		UpdateText ();
	}

	public void UpdateText(){
		scoresText.text = "Scores: " + currentScores.ToString ();
	}
}
