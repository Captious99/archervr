﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHitmarker : MonoBehaviour {

	private float liveTime;

	void Update () {
		liveTime += Time.deltaTime;
		if (liveTime >= 0.5f) {
			liveTime = 0;
			this.gameObject.SetActive (false);
		}
	}
}
