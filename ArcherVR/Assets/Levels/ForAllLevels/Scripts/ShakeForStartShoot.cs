﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeForStartShoot : MonoBehaviour {
	public GameObject myCamera;
	public ThrowArrow throwArrow;

	private float previousAngle;
	public float[] rates = new float[10];
	private int count = 0;
	private bool findSred = false;
	public float sred;

	public float maxForReact = 1.5f;
		

	void Update () {
		maxForReact = 0.7f + ((float)GameObject.FindWithTag ("Settings").GetComponent<Settings> ().GetSensShoot ()) / 10;
		ShakeHead ();

	}

	private void ShakeHead () {
		float angle = myCamera.transform.localRotation.x;
		//float deltaAngle = previousAngle - angle;
        /////
        float deltaAngle = angle - previousAngle;
        /////
		float rate = deltaAngle / Time.deltaTime;
		previousAngle = angle;
		addInArray (rate);
		sredNumber ();
		if (sred > maxForReact) {
			///
			if (!throwArrow.shoot) {
				throwArrow.shoot = true;
				ResetParametrs ();
			} else {
				throwArrow.shoot = false;
				ResetParametrs ();
			}
			///
			//throwArrow.shoot = true;
			//ResetParametrs ();
			///
		}
	}

	private void ResetParametrs (){
		rates = new float[10] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		count = 0;
		findSred = false;
		sred = 0;
	}

	private void addInArray (float a){
		if (a < 0) {
			a = -a;
			rates [count] = a;
			count++;
			if (count == rates.Length) {
				count = 0;
				findSred = true;
			}
		} else {
			ResetParametrs ();
		}
	}

	private void sredNumber (){
		if (findSred) {
			int newCount = count;
			for (int i = 0; i <= (rates.Length-3); i++) {
				newCount--;
				if (newCount == -1) {
					newCount = (rates.Length-1);
				}
				sred += rates [newCount];
			}
			sred = sred / (rates.Length-1);
		}
	}
}