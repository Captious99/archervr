﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorTargets : MonoBehaviour {
	public GameObject[] spawnPoints = new GameObject [30];
	public bool[] fullSpawnPoint = new bool[30];

	public GameObject archer;

	public GameObject trainingTarget;
	public GameObject bigTarget;
	public GameObject coinTarget;
	public GameObject ammoTarget;
	public GameObject timeTarget;

	public int chanceTrainTarget; //0-59
	public int chanceBigTarget; //60-74
	public int chanceCoinTarget; //75-79
	public int chanceAmmoTarget; //80-89
	public int chanceTimeTarget; //90-100

	public GameObject addAmmoText;
	public GameObject addTimeText;
	public GameObject addScoresText;
	public GameObject addGoldText;

	public float timeSpawn;//иапозон времени в течении которого спавн уменьшается в течение времени 5c
	private float spawnTaimer;

	public float timeLive;//время жизни цели уменьшается с течением времени 4c

	public float maxTarget;//максимальное количество целей увеличивается с течением времени
	public int currentTargets;

	public float minusChangeTimeSpawn;
	public float minusChangeTimeLive;
	public float addChangeMaxTarget;

	void Update () {
		SpawnTarget ();
	}

	private void SpawnTarget(){
		if(currentTargets < (int) maxTarget){
			spawnTaimer += Time.deltaTime;
			if (spawnTaimer >= timeSpawn) {
				//SPAWN
				SetTargetOnPoint(FindTarget());
				//
				spawnTaimer = 0;
				if (timeSpawn > 1.0f) {
					timeSpawn -= minusChangeTimeSpawn;
				}
				if (timeLive > 3f) {
					timeLive -= minusChangeTimeLive;
				}
				if (maxTarget < 10.0f) {
					maxTarget += addChangeMaxTarget;
				}
			}
		}
	}

	private GameObject FindTarget(){
		int r = Random.Range(0,101);
		if (r >= 0 && r < chanceTrainTarget) {
			return trainingTarget;
		} else if (r >= chanceTrainTarget && r < chanceBigTarget) {
			return bigTarget;
		} else if (r >= chanceBigTarget && r < chanceCoinTarget) {
			return coinTarget;
		} else if (r >= chanceCoinTarget && r < chanceAmmoTarget) {
			return ammoTarget;
		} else if (r >= chanceAmmoTarget && r <= chanceTimeTarget) {
			return timeTarget;
		}
		return null;
	}

	private void SetTargetOnPoint(GameObject target){
		int r = Random.Range(0,spawnPoints.Length);
		while (fullSpawnPoint [r] == true) {
			r++;
			if (r == spawnPoints.Length) {
				r = 0;
			}
		}
		target.GetComponent<Target> ().SetNumber (r);
		target.GetComponent<Target> ().SetTimeLive (timeLive);
		target.GetComponent<Target> ().SetArcher (archer);
		target.GetComponent<Target> ().SetGenerator (this.gameObject);
		target.GetComponent<Target> ().SetAddTexts (addAmmoText, addTimeText, addScoresText, addGoldText);
		//GameObject t = target;
		Instantiate (target.transform, spawnPoints[r].transform.position, spawnPoints[r].transform.rotation);
		Debug.Log("Spawn " + target.name + " in " + r.ToString());
		fullSpawnPoint[r] = true;
		currentTargets++;
	}
}