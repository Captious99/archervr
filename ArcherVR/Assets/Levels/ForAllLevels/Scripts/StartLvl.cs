﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLvl : MonoBehaviour {
	public GameObject aim;
	public GameObject aimLoad;
	public GameObject Texts;
	public GameObject startText;
	public GameObject archer;
	public GameObject generator;

	public float textTimer;

	void Start () {
		aim.SetActive (false);
		aimLoad.SetActive (false);
		Texts.SetActive (false);
		startText.SetActive (true);
		generator.SetActive (false);
		archer.GetComponent<ShakeForStartShoot> ().enabled = false;
		archer.GetComponent<TimeForEnd> ().enabled = false;
	}

	void Update () {
		aim.SetActive (false);
		textTimer += Time.deltaTime;
		if (textTimer <= 1.0f) {
			startText.GetComponent<TextMesh>().text = "3";
		} else if (textTimer > 1.0f && textTimer <= 2.0f) {
			startText.GetComponent<TextMesh>().text = "2";
		} else if (textTimer > 2.0f && textTimer <= 3.0f) {
			startText.GetComponent<TextMesh>().text = "1";
		} else if (textTimer > 3.0f && textTimer <= 4.0f) {
			startText.GetComponent<TextMesh>().text = "Start";
		} else if (textTimer > 4.0f) {
			aim.SetActive (true);
			aimLoad.SetActive (true);
			Texts.SetActive (true);
			startText.SetActive (false);
			generator.SetActive (true);
			archer.GetComponent<ShakeForStartShoot> ().enabled = true;
			archer.GetComponent<TimeForEnd> ().enabled = true;
			this.gameObject.SetActive (false);
		}
	}
}
