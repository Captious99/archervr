﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseAddText : MonoBehaviour {

	private float closeTimer = 0;

	void Start () {
		//this.gameObject.SetActive (false);
	}

	void Update () {
		closeTimer += Time.deltaTime;
		if (closeTimer >= 2.0f) {
			closeTimer = 0;
			this.gameObject.SetActive (false);
		}
	}
}
