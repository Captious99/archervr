﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
	public GameObject arrowHurt;
	public float speedRotate;
	public int damage;
	private GameObject hitMarker;

	private float rotateTime;
	private float liveTime;

	private Ammo amm;

	void Start(){
		damage = 1 + GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ().GetDamage();
		hitMarker = GameObject.Find ("Archer").GetComponent<ThrowArrow> ().hitMarker;
		amm = GameObject.Find ("Archer").GetComponent<Ammo> ();
	}

	void Update () {
		RotateSpear ();
		SpearHurt ();
		LiveTime ();
	}

	private void LiveTime(){
		liveTime += Time.deltaTime;
		if (liveTime >= 10.0f) {
			if (amm.currentAmmo == 0) {
				GameObject.Find("EndGameManager").GetComponent<EndLevel>().GameOver ();
			}
			Destroy (this.gameObject);
		}
	}

	private void SpearHurt(){
		RaycastHit hit;
		Ray ray = new Ray (this.transform.position, this.transform.rotation * Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			if(hit.distance <= 1f){
				Transform arrow = (Transform) Instantiate (arrowHurt.transform, hit.point, this.transform.rotation);
				if(hit.transform.gameObject.tag == "Target"){
					SoundHit ();
					arrow.SetParent (hit.transform);
					Debug.Log ("Shoot");
					hitMarker.gameObject.SetActive (true);
					hit.transform.gameObject.GetComponent<Target> ().takeDamage (damage);
				} else if (hit.transform.gameObject.tag == "Fruit"){
					SoundHit ();
					arrow.SetParent (hit.transform);
					Debug.Log ("Shoot");
					hitMarker.gameObject.SetActive (true);
					hit.transform.gameObject.GetComponent<Fruit> ().TakeDamage (damage);
				}
				if (amm.currentAmmo == 0) {
					GameObject.Find("EndGameManager").GetComponent<EndLevel>().GameOver ();
				}
				Destroy (this.gameObject);
			}
		}
	}

	private void RotateSpear(){
		if(rotateTime < 90.0f/speedRotate){
			GetComponent<Transform>().rotation *= Quaternion.Euler (Time.deltaTime*speedRotate, 0 ,0);
			rotateTime += Time.deltaTime;
		}
	}

	private void SoundHit(){
		GameObject.Find ("HitSound").GetComponent<AudioSource> ().Play ();
	}
}