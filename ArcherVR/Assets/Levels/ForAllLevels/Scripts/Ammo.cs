﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {
	public int startAmmo;
	public int currentAmmo;
	public EndLevel end;

	public TextMesh ammoText;

	void Start(){
		currentAmmo = startAmmo;
		UpdateText ();
	}

	/*void Update () {
		if (currentAmmo < 0) {
			end.GameOver ();
		}
	}*/

	public void UpdateText(){
		ammoText.text = "Arrows: " + currentAmmo.ToString ();
	}
}