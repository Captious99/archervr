﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThrowArrow : MonoBehaviour {
	public Transform arrow;
	public GameObject spawn;
	public Ammo ammo;
	public GameObject hitMarker;

	public GameObject bow;
	public GameObject arrowInBow;

	public int forceThrow;
	public float needTime;

	public Image aim;
	public Image aimLoad;
	public bool shoot;

	private float throwTimer;
	private bool isShoting;
	private bool isAnim;
	private float timerForShoting;

	void Start(){
		needTime = 3.5f - ((float)GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ().GetSpeed()) * 0.2f;
		forceThrow = 800 + GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ().GetPower() * 100;
	}

	void Update () {
		if (isShoting) {
			timerForShoting += Time.deltaTime;
			if(!isAnim && needTime - timerForShoting <= 0.85f){
				bow.GetComponent<Animator>().Play ("Armature|shoot");
				isAnim = true;
			}
			/*if(needTime - timerForShoting <= 0.09f){
				arrowInBow.SetActive (false);
			}*/
		}
		///
		else {
			timerForShoting = 0;
		}
		///
		if (shoot) {
			isShoting = true;
			arrowInBow.SetActive (true);
			aim.gameObject.SetActive (false);
			aimLoad.fillAmount = throwTimer / needTime;
			throwTimer += Time.deltaTime;
			if (throwTimer >= needTime) {
				arrowInBow.SetActive (false);
				ArrowThrow ();
				ammo.currentAmmo--;
				ammo.UpdateText ();
				throwTimer = 0;
				aim.gameObject.SetActive (true);
				aimLoad.fillAmount = 0;
				shoot = false;
				isShoting = false;
				isAnim = false;
				timerForShoting = 0;
			}
		}
		///
		else {
			isShoting = false;
			arrowInBow.SetActive (false);
			aim.gameObject.SetActive (true);
			//aimLoad.fillAmount = 0;
			throwTimer = 0;
		}
		///
	}

	private void ArrowThrow(){
		SoundArrow ();
		Transform SpearInstance = (Transform)Instantiate (arrow, spawn.transform.position, spawn.transform.rotation); 
		SpearInstance.GetComponent<Rigidbody> ().AddForce (spawn.transform.forward * forceThrow); 
	}

	private void SoundArrow(){
		GameObject.Find ("ArrowSound").GetComponent<AudioSource> ().Play ();
	}
}