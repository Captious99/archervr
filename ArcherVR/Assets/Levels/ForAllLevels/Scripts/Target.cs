﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
	public bool dopTarget;

	public int health;
	public int scoresForKill;
	public int ammoForKill;
	public float timeForKill;
	public int goldForKill;

	public Scores scores;
	public Ammo ammo;
	public TimeForEnd timefe;
	public GeneratorTargets gerT;

	public float timeLive;
	public int number;

	public GameObject addAmmoText;
	public GameObject addTimeText;
	public GameObject addScoresText;
	public GameObject addGoldText;

	public bool bigTarget;

	void Start(){
		int damage = 1+GameObject.FindWithTag ("BowSkills").GetComponent<BowSkills> ().GetDamage ();
		if (bigTarget) {
			health = damage + Random.Range (1, 20);
		} else {
			health = damage;
		}
		if (dopTarget) {
			Target parentTar = this.transform.parent.gameObject.GetComponent<Target> ();
			scores = parentTar.scores;
			ammo = parentTar.ammo;
			timefe = parentTar.timefe;
			gerT = parentTar.gerT;
			addAmmoText = parentTar.addAmmoText;
			addTimeText = parentTar.addTimeText;
			addScoresText = parentTar.addScoresText;
			addGoldText = parentTar.addGoldText;
		}
	}

	public void SetAddTexts(GameObject ammoT, GameObject timeT, GameObject scoresT, GameObject goldT){
		addAmmoText = ammoT;
		addTimeText = timeT;
		addScoresText = scoresT;
		addGoldText = goldT;
	}

	public void SetNumber (int n){
		number = n;
	}
	public void SetTimeLive (float n){
		timeLive = n;
	}
	public void SetArcher(GameObject archer){
		scores = archer.GetComponent<Scores> ();
		ammo = archer.GetComponent<Ammo> ();
		timefe = archer.GetComponent<TimeForEnd> ();
	}
	public void SetGenerator(GameObject gener){
		gerT = gener.GetComponent<GeneratorTargets> ();
	}

	void Update(){
		LiveTime ();	
	}

	private void LiveTime(){
		timeLive -= Time.deltaTime;
		if (timeLive <= 0) {
			deadForGenerator ();
			if (dopTarget) {
				GameObject thisGaOb = this.gameObject;
				GameObject parentGamOb = thisGaOb.transform.parent.gameObject;
				Destroy (this.gameObject);
				Destroy (parentGamOb);
			} else {
				Destroy (this.gameObject);
			}
		}
	}

	public void takeDamage(int damage){
		health -= damage;
		dead ();
	}

	private void dead(){
		if (health <= 0) {
			scores.currentScores += scoresForKill;
			scores.UpdateText ();
			ammo.currentAmmo += ammoForKill;
			ammo.UpdateText ();
			timefe.currentTime += timeForKill;
			deadForGenerator ();
			GameObject.FindWithTag ("Gold").GetComponent<Gold> ().AddGold (goldForKill);
			if (ammoForKill > 0) {
				addAmmoText.SetActive (true);
				addAmmoText.GetComponent<TextMesh> ().text = "Ammo: + " + ammoForKill.ToString ();
			}
			if (timeForKill > 0) {
				addTimeText.SetActive (true);
				addTimeText.GetComponent<TextMesh> ().text = "Time: + " + timeForKill.ToString ();
			}
			if (scoresForKill > 0) {
				addScoresText.SetActive (true);
				addScoresText.GetComponent<TextMesh> ().text = "Scores: + " + scoresForKill.ToString ();
			}
			if (goldForKill > 0) {
				addGoldText.SetActive (true);
				addGoldText.GetComponent<TextMesh> ().text = "Gold: + " + goldForKill.ToString ();
			}
			if (dopTarget) {
				GameObject thisGaOb = this.gameObject;
				GameObject parentGamOb = thisGaOb.transform.parent.gameObject;
				Destroy (this.gameObject);
				Destroy (parentGamOb);
			} else {
				Destroy (this.gameObject);
			}
		}
	}

	private void deadForGenerator(){
		gerT.currentTargets--;
		gerT.fullSpawnPoint [number] = false;
	}
}