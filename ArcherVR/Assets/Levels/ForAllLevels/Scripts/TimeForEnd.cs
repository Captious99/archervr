﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeForEnd : MonoBehaviour {
	public float currentTime;
	public float startTime;
	public EndLevel end;

	public TextMesh timeText;

	void Start () {
		currentTime = startTime;
	}

	void Update () {
		currentTime -= Time.deltaTime;
		UpdateText (); 
		EndTime ();
	}

	private void UpdateText(){
		timeText.text = "Time: " + ((int)currentTime).ToString () + " s";
	}

	private void EndTime(){
		if (currentTime <= 0) {
			end.GameOver ();
		}
	}
}
