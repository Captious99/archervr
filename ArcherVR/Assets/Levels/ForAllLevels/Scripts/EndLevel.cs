﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevel : MonoBehaviour {
	public GameObject myCamera;
	public GameObject endMeny;
	public TextMesh scoresText;
	public TextMesh bestScoresText;
	public Scores scores;
	public GameObject archer;
	public GameObject textAmmo;
	public GameObject textScores;
	public GameObject textTime;
	public GameObject addGoldText;
	public GameObject returnButton;
	public GameObject menyButton;
	public Image aimLoad;

	private bool endGame;
	private float taimerMeny;
	private float taimerReturn;
	public int numberThisLvl;

	public int scoresForGold;

	public enum GameMode{Training, Fruit, Target};
	public GameMode thisGameMode;

	void Update () {
		if (endGame) {
			Button ();
		}
	}

	private void Button(){
		RaycastHit hit;
		Ray ray = new Ray (myCamera.transform.position, myCamera.transform.rotation * Vector3.forward);
		if (Physics.Raycast(ray, out hit)) {
			Transform objectHit = hit.transform;
			if (objectHit.gameObject == returnButton) {
				taimerMeny += Time.deltaTime;
				aimLoad.fillAmount = taimerMeny / 2.0f;
				if (taimerMeny >= 2f) {
					SoundButton ();
					aimLoad.fillAmount = 0;
					taimerMeny = 0;
					Application.LoadLevel (numberThisLvl);
				}
			} else if (objectHit.gameObject == menyButton) {
				taimerMeny += Time.deltaTime;
				aimLoad.fillAmount = taimerMeny / 2.0f;
				if (taimerMeny >= 2f) {
					SoundButton ();
					aimLoad.fillAmount = 0;
					taimerMeny = 0;
					Application.LoadLevel (0);
				}
			} else {
				taimerMeny = 0;
				aimLoad.fillAmount = 0;
			}
		}
	}

	public void GameOver(){
		endMeny.SetActive (true);
		scoresText.text = "Scores: " + scores.currentScores;
		if (thisGameMode == GameMode.Training) {
			bool isBest = GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().TakeScore (scores.currentScores, BestScore.GameMode.Training);
			if (isBest) {
				bestScoresText.gameObject.SetActive (true);
			} else {
				bestScoresText.gameObject.SetActive (false);
			}
		} else if (thisGameMode == GameMode.Fruit) {
			bool isBest = GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().TakeScore (scores.currentScores, BestScore.GameMode.Fruit);
			if (isBest) {
				bestScoresText.gameObject.SetActive (true);
			} else {
				bestScoresText.gameObject.SetActive (false);
			}
		} else if (thisGameMode == GameMode.Target) {
			bool isBest = GameObject.FindWithTag ("BestScore").GetComponent<BestScore> ().TakeScore (scores.currentScores, BestScore.GameMode.Target);
			if (isBest) {
				bestScoresText.gameObject.SetActive (true);
			} else {
				bestScoresText.gameObject.SetActive (false);
			}
		}
		int coinsForEnd = (int)scores.currentScores / scoresForGold;
		if (coinsForEnd > 0) {
			GameObject.FindWithTag ("Gold").GetComponent<Gold> ().AddGold (coinsForEnd);
			addGoldText.SetActive (true);
			addGoldText.GetComponent<TextMesh> ().text = "Gold: +" + coinsForEnd.ToString ();
		} else {
			addGoldText.SetActive (false);
		}
		archer.GetComponent<ShakeForStartShoot> ().enabled = false;
		archer.GetComponent<Ammo> ().enabled = false;
		archer.GetComponent<Scores> ().enabled = false;
		archer.GetComponent<TimeForEnd> ().enabled = false;
		textAmmo.SetActive (false);
		textScores.SetActive (false);
		textTime.SetActive (false);
		endGame = true;
	}

	private void SoundButton(){
		GameObject.Find ("ButtonSound").GetComponent<AudioSource> ().Play ();
	}
}
