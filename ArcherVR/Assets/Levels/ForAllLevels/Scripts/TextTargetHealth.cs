﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTargetHealth : MonoBehaviour {

	private TextMesh textHealth;
	private GameObject archer;
	private GameObject target;
	private bool isFruit;

	void Start () {
		archer = GameObject.Find ("Archer");
		target = this.transform.parent.gameObject;
		textHealth = this.gameObject.GetComponent<TextMesh> ();
		if (target.tag == "Fruit") {
			isFruit = true;
		} else if (target.tag == "Target") {
			isFruit = false;
		}
	}

	void Update () {
		if (!isFruit) {
			textHealth.text = "Health: " + target.GetComponent<Target> ().health;
		} else {
			textHealth.text = "Health: " + target.GetComponent<Fruit> ().health;
		}
		this.transform.LookAt (archer.transform);
		this.transform.rotation *= Quaternion.Euler (1, 180, 1);
	}
}
