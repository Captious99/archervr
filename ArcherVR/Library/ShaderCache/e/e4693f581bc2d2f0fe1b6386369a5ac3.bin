2O                         DIRECTIONAL    SHADOWS_SCREEN  
   _NORMALMAP     _METALLICGLOSSMAP      _PARALLAXMAPH&     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    half4 _WorldSpaceLightPos0;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    float4 hlslcc_mtx4x4unity_WorldToShadow[16];
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 unity_SpecCube0_HDR;
    half4 _LightColor0;
    half4 _Color;
    half _BumpScale;
    half _GlossMapScale;
    half _OcclusionStrength;
    half _Parallax;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float3 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler samplerunity_NHxRoughness [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    sampler sampler_BumpMap [[ sampler (3) ]],
    sampler sampler_MetallicGlossMap [[ sampler (4) ]],
    sampler sampler_OcclusionMap [[ sampler (5) ]],
    sampler sampler_ParallaxMap [[ sampler (6) ]],
    sampler sampler_ShadowMapTexture [[ sampler (7) ]],
    texture2d<half, access::sample > _ParallaxMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (1) ]] ,
    texture2d<half, access::sample > _MetallicGlossMap [[ texture (2) ]] ,
    texture2d<half, access::sample > _BumpMap [[ texture (3) ]] ,
    texture2d<half, access::sample > _OcclusionMap [[ texture (4) ]] ,
    texture2d<half, access::sample > unity_NHxRoughness [[ texture (5) ]] ,
    texturecube<half, access::sample > unity_SpecCube0 [[ texture (6) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture (7) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half3 u_xlat16_0;
    float3 u_xlat1;
    half4 u_xlat16_1;
    half3 u_xlat16_2;
    half4 u_xlat16_3;
    float3 u_xlat4;
    half3 u_xlat16_4;
    half4 u_xlat16_5;
    half4 u_xlat16_6;
    half3 u_xlat16_7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    half2 u_xlat16_10;
    half3 u_xlat16_11;
    float3 u_xlat12;
    half u_xlat16_12;
    half2 u_xlat16_22;
    half2 u_xlat16_23;
    half u_xlat16_33;
    half u_xlat16_35;
    half u_xlat16_38;
    u_xlat16_0.x = input.TEXCOORD2.w;
    u_xlat16_0.y = input.TEXCOORD3.w;
    u_xlat16_0.z = input.TEXCOORD4.w;
    u_xlat16_33 = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_33 = rsqrt(u_xlat16_33);
    u_xlat16_0.xy = half2(u_xlat16_33) * u_xlat16_0.xy;
    u_xlat16_22.x = fma(u_xlat16_0.z, u_xlat16_33, half(0.419999987));
    u_xlat16_0.xy = u_xlat16_0.xy / u_xlat16_22.xx;
    u_xlat16_1.x = _ParallaxMap.sample(sampler_ParallaxMap, input.TEXCOORD0.xy).y;
    u_xlat16_22.x = Globals._Parallax * half(0.5);
    u_xlat16_22.x = fma(u_xlat16_1.x, Globals._Parallax, (-u_xlat16_22.x));
    u_xlat1.xy = fma(float2(u_xlat16_22.xx), float2(u_xlat16_0.xy), input.TEXCOORD0.xy);
    u_xlat16_23.x = _OcclusionMap.sample(sampler_OcclusionMap, u_xlat1.xy).y;
    u_xlat16_0.x = (-Globals._OcclusionStrength) + half(1.0);
    u_xlat16_0.x = fma(u_xlat16_23.x, Globals._OcclusionStrength, u_xlat16_0.x);
    u_xlat16_23.xy = _MetallicGlossMap.sample(sampler_MetallicGlossMap, u_xlat1.xy).xw;
    u_xlat16_2.z = fma((-u_xlat16_23.y), Globals._GlossMapScale, half(1.0));
    u_xlat16_11.x = fma((-u_xlat16_2.z), half(0.699999988), half(1.70000005));
    u_xlat16_11.x = u_xlat16_11.x * u_xlat16_2.z;
    u_xlat16_11.x = u_xlat16_11.x * half(6.0);
    u_xlat16_3.xyz = _BumpMap.sample(sampler_BumpMap, u_xlat1.xy).xyz;
    u_xlat16_4.xyz = _MainTex.sample(sampler_MainTex, u_xlat1.xy).xyz;
    u_xlat16_5.xyz = fma(u_xlat16_3.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -1.0));
    u_xlat16_22.xy = u_xlat16_5.xy * half2(Globals._BumpScale);
    u_xlat16_5.xyw = u_xlat16_22.yyy * input.TEXCOORD3.xyz;
    u_xlat16_5.xyw = fma(input.TEXCOORD2.xyz, u_xlat16_22.xxx, u_xlat16_5.xyw);
    u_xlat16_5.xyz = fma(input.TEXCOORD4.xyz, u_xlat16_5.zzz, u_xlat16_5.xyw);
    u_xlat16_22.x = dot(u_xlat16_5.xyz, u_xlat16_5.xyz);
    u_xlat16_22.x = rsqrt(u_xlat16_22.x);
    u_xlat16_3.xyz = u_xlat16_22.xxx * u_xlat16_5.xyz;
    u_xlat16_22.x = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_22.x = rsqrt(u_xlat16_22.x);
    u_xlat16_5.xyz = u_xlat16_22.xxx * input.TEXCOORD1.xyz;
    u_xlat16_22.x = dot(u_xlat16_5.xyz, u_xlat16_3.xyz);
    u_xlat16_22.x = u_xlat16_22.x + u_xlat16_22.x;
    u_xlat16_6.xyz = fma(u_xlat16_3.xyz, (-u_xlat16_22.xxx), u_xlat16_5.xyz);
    u_xlat16_6 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_6.xyz), level(float(u_xlat16_11.x)));
    u_xlat16_11.x = u_xlat16_6.w + half(-1.0);
    u_xlat16_11.x = fma(Globals.unity_SpecCube0_HDR.w, u_xlat16_11.x, half(1.0));
    u_xlat16_11.x = u_xlat16_11.x * Globals.unity_SpecCube0_HDR.x;
    u_xlat16_11.xyz = u_xlat16_6.xyz * u_xlat16_11.xxx;
    u_xlat16_11.xyz = u_xlat16_0.xxx * u_xlat16_11.xyz;
    u_xlat16_35 = fma((-u_xlat16_23.x), half(0.779083729), half(0.779083729));
    u_xlat16_38 = fma(u_xlat16_23.y, Globals._GlossMapScale, (-u_xlat16_35));
    u_xlat16_38 = u_xlat16_38 + half(1.0);
    u_xlat16_38 = clamp(u_xlat16_38, 0.0h, 1.0h);
    u_xlat16_7.xyz = fma(Globals._Color.xyz, u_xlat16_4.xyz, half3(-0.220916301, -0.220916301, -0.220916301));
    u_xlat16_1.xyw = u_xlat16_4.xyz * Globals._Color.xyz;
    u_xlat16_8.xyz = half3(u_xlat16_35) * u_xlat16_1.xyw;
    u_xlat16_7.xyz = fma(u_xlat16_23.xxx, u_xlat16_7.xyz, half3(0.220916301, 0.220916301, 0.220916301));
    u_xlat16_9.xyz = half3(u_xlat16_38) + (-u_xlat16_7.xyz);
    u_xlat16_35 = dot((-u_xlat16_5.xyz), u_xlat16_3.xyz);
    u_xlat16_38 = u_xlat16_35 + u_xlat16_35;
    u_xlat16_35 = u_xlat16_35;
    u_xlat16_35 = clamp(u_xlat16_35, 0.0h, 1.0h);
    u_xlat16_10.y = (-u_xlat16_35) + half(1.0);
    u_xlat16_5.xyz = fma(u_xlat16_3.xyz, (-half3(u_xlat16_38)), (-u_xlat16_5.xyz));
    u_xlat16_10.x = dot(u_xlat16_5.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_5.xy = u_xlat16_10.xy * u_xlat16_10.xy;
    u_xlat16_2.xy = u_xlat16_5.xy * u_xlat16_5.xy;
    u_xlat16_5.xyz = fma(u_xlat16_2.yyy, u_xlat16_9.xyz, u_xlat16_7.xyz);
    u_xlat16_1.x = unity_NHxRoughness.sample(samplerunity_NHxRoughness, float2(u_xlat16_2.xz)).x;
    u_xlat16_1.x = u_xlat16_1.x * half(16.0);
    u_xlat16_2.xyz = fma(u_xlat16_1.xxx, u_xlat16_7.xyz, u_xlat16_8.xyz);
    u_xlat16_11.xyz = u_xlat16_11.xyz * u_xlat16_5.xyz;
    u_xlat16_3.w = half(1.0);
    u_xlat16_5.x = dot(Globals.unity_SHAr, u_xlat16_3);
    u_xlat16_5.y = dot(Globals.unity_SHAg, u_xlat16_3);
    u_xlat16_5.z = dot(Globals.unity_SHAb, u_xlat16_3);
    u_xlat16_35 = dot(u_xlat16_3.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_35 = clamp(u_xlat16_35, 0.0h, 1.0h);
    u_xlat16_5.xyz = u_xlat16_5.xyz + input.TEXCOORD5.xyz;
    u_xlat16_5.xyz = max(u_xlat16_5.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_5.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * half3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = fma(u_xlat16_1.xyz, half3(1.05499995, 1.05499995, 1.05499995), half3(-0.0549999997, -0.0549999997, -0.0549999997));
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_5.xyz = u_xlat16_0.xxx * u_xlat16_1.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_5.xyz, u_xlat16_8.xyz, u_xlat16_11.xyz);
    u_xlat1.xyz = input.TEXCOORD8.xyz + (-Globals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = sqrt(u_xlat1.x);
    u_xlat12.xyz = (-input.TEXCOORD8.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat4.x = Globals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat4.y = Globals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat4.z = Globals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat12.x = dot(u_xlat12.xyz, u_xlat4.xyz);
    u_xlat1.x = (-u_xlat12.x) + u_xlat1.x;
    u_xlat1.x = fma(Globals.unity_ShadowFadeCenterAndType.w, u_xlat1.x, u_xlat12.x);
    u_xlat1.x = fma(u_xlat1.x, float(Globals._LightShadowData.z), float(Globals._LightShadowData.w));
    u_xlat1.x = clamp(u_xlat1.x, 0.0f, 1.0f);
    u_xlat12.xyz = input.TEXCOORD8.yyy * Globals.hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat12.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToShadow[0].xyz, input.TEXCOORD8.xxx, u_xlat12.xyz);
    u_xlat12.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToShadow[2].xyz, input.TEXCOORD8.zzz, u_xlat12.xyz);
    u_xlat12.xyz = u_xlat12.xyz + Globals.hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    u_xlat16_12 = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat12.xy, saturate(u_xlat12.z), level(0.0));
    u_xlat16_33 = (-Globals._LightShadowData.x) + half(1.0);
    u_xlat16_33 = fma(u_xlat16_12, u_xlat16_33, Globals._LightShadowData.x);
    u_xlat16_33 = half(u_xlat1.x + float(u_xlat16_33));
    u_xlat16_33 = clamp(u_xlat16_33, 0.0h, 1.0h);
    u_xlat16_5.xyz = half3(u_xlat16_33) * Globals._LightColor0.xyz;
    u_xlat16_5.xyz = half3(u_xlat16_35) * u_xlat16_5.xyz;
    output.SV_Target0.xyz = fma(u_xlat16_2.xyz, u_xlat16_5.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = half(1.0);
    return output;
}
                          Globals �        _WorldSpaceCameraPos                         _WorldSpaceLightPos0                    
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                   (      _LightShadowData                 0     unity_ShadowFadeCenterAndType                     @     unity_SpecCube0_HDR                  �     _LightColor0                 �     _Color                   �  
   _BumpScale                   �     _GlossMapScale                   �     _OcclusionStrength                   �  	   _Parallax                    �     unity_WorldToShadow                 0      unity_MatrixV                    P  	      _ParallaxMap             _MainTex            _MetallicGlossMap               _BumpMap            _OcclusionMap               unity_NHxRoughness              unity_SpecCube0              _ShadowMapTexture               Globals            