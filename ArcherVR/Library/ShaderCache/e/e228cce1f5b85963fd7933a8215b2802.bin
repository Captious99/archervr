2O                         DIRECTIONAL    SHADOWS_SCREEN     DIRLIGHTMAP_COMBINED   DYNAMICLIGHTMAP_ON     _METALLICGLOSSMAP      _PARALLAXMAP4%     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    half4 _WorldSpaceLightPos0;
    float4 hlslcc_mtx4x4unity_WorldToShadow[16];
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 unity_SpecCube0_HDR;
    half4 unity_DynamicLightmap_HDR;
    half4 _LightColor0;
    half4 _Color;
    half _GlossMapScale;
    half _OcclusionStrength;
    half _Parallax;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float3 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    sampler samplerunity_DynamicLightmap [[ sampler (0) ]],
    sampler samplerunity_SpecCube0 [[ sampler (1) ]],
    sampler samplerunity_NHxRoughness [[ sampler (2) ]],
    sampler sampler_MainTex [[ sampler (3) ]],
    sampler sampler_MetallicGlossMap [[ sampler (4) ]],
    sampler sampler_OcclusionMap [[ sampler (5) ]],
    sampler sampler_ParallaxMap [[ sampler (6) ]],
    sampler sampler_ShadowMapTexture [[ sampler (7) ]],
    texture2d<half, access::sample > _ParallaxMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (1) ]] ,
    texture2d<half, access::sample > _MetallicGlossMap [[ texture (2) ]] ,
    texture2d<half, access::sample > _OcclusionMap [[ texture (3) ]] ,
    texture2d<half, access::sample > unity_NHxRoughness [[ texture (4) ]] ,
    texture2d<half, access::sample > unity_DynamicLightmap [[ texture (5) ]] ,
    texture2d<half, access::sample > unity_DynamicDirectionality [[ texture (6) ]] ,
    texturecube<half, access::sample > unity_SpecCube0 [[ texture (7) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture (8) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half3 u_xlat16_0;
    float3 u_xlat1;
    half4 u_xlat16_1;
    float3 u_xlat2;
    half3 u_xlat16_2;
    half3 u_xlat16_3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    half4 u_xlat16_6;
    half3 u_xlat16_7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    float3 u_xlat10;
    half u_xlat16_10;
    half3 u_xlat16_13;
    half u_xlat16_18;
    half2 u_xlat16_19;
    half u_xlat16_27;
    half u_xlat16_30;
    half u_xlat16_31;
    u_xlat16_0.x = input.TEXCOORD2.w;
    u_xlat16_0.y = input.TEXCOORD3.w;
    u_xlat16_0.z = input.TEXCOORD4.w;
    u_xlat16_27 = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_27 = rsqrt(u_xlat16_27);
    u_xlat16_0.xy = half2(u_xlat16_27) * u_xlat16_0.xy;
    u_xlat16_18 = fma(u_xlat16_0.z, u_xlat16_27, half(0.419999987));
    u_xlat16_0.xy = u_xlat16_0.xy / half2(u_xlat16_18);
    u_xlat16_1.x = _ParallaxMap.sample(sampler_ParallaxMap, input.TEXCOORD0.xy).y;
    u_xlat16_18 = Globals._Parallax * half(0.5);
    u_xlat16_18 = fma(u_xlat16_1.x, Globals._Parallax, (-u_xlat16_18));
    u_xlat1.xy = fma(float2(u_xlat16_18), float2(u_xlat16_0.xy), input.TEXCOORD0.xy);
    u_xlat16_19.x = _OcclusionMap.sample(sampler_OcclusionMap, u_xlat1.xy).y;
    u_xlat16_0.x = (-Globals._OcclusionStrength) + half(1.0);
    u_xlat16_0.x = fma(u_xlat16_19.x, Globals._OcclusionStrength, u_xlat16_0.x);
    u_xlat16_19.xy = _MetallicGlossMap.sample(sampler_MetallicGlossMap, u_xlat1.xy).xw;
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, u_xlat1.xy).xyz;
    u_xlat16_3.z = fma((-u_xlat16_19.y), Globals._GlossMapScale, half(1.0));
    u_xlat16_9.x = fma((-u_xlat16_3.z), half(0.699999988), half(1.70000005));
    u_xlat16_9.x = u_xlat16_9.x * u_xlat16_3.z;
    u_xlat16_9.x = u_xlat16_9.x * half(6.0);
    u_xlat16_18 = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_18 = rsqrt(u_xlat16_18);
    u_xlat16_4.xyz = half3(u_xlat16_18) * input.TEXCOORD1.xyz;
    u_xlat16_18 = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat16_18 = rsqrt(u_xlat16_18);
    u_xlat16_5.xyz = half3(u_xlat16_18) * input.TEXCOORD4.xyz;
    u_xlat16_18 = dot(u_xlat16_4.xyz, u_xlat16_5.xyz);
    u_xlat16_18 = u_xlat16_18 + u_xlat16_18;
    u_xlat16_6.xyz = fma(u_xlat16_5.xyz, (-half3(u_xlat16_18)), u_xlat16_4.xyz);
    u_xlat16_6 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_6.xyz), level(float(u_xlat16_9.x)));
    u_xlat16_9.x = u_xlat16_6.w + half(-1.0);
    u_xlat16_9.x = fma(Globals.unity_SpecCube0_HDR.w, u_xlat16_9.x, half(1.0));
    u_xlat16_9.x = u_xlat16_9.x * Globals.unity_SpecCube0_HDR.x;
    u_xlat16_9.xyz = u_xlat16_6.xyz * u_xlat16_9.xxx;
    u_xlat16_9.xyz = u_xlat16_0.xxx * u_xlat16_9.xyz;
    u_xlat16_30 = dot((-u_xlat16_4.xyz), u_xlat16_5.xyz);
    u_xlat16_31 = u_xlat16_30 + u_xlat16_30;
    u_xlat16_30 = u_xlat16_30;
    u_xlat16_30 = clamp(u_xlat16_30, 0.0h, 1.0h);
    u_xlat16_7.y = (-u_xlat16_30) + half(1.0);
    u_xlat16_4.xyz = fma(u_xlat16_5.xyz, (-half3(u_xlat16_31)), (-u_xlat16_4.xyz));
    u_xlat16_7.x = dot(u_xlat16_4.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_4.xy = u_xlat16_7.xy * u_xlat16_7.xy;
    u_xlat16_3.xy = u_xlat16_4.xy * u_xlat16_4.xy;
    u_xlat16_30 = fma((-u_xlat16_19.x), half(0.779083729), half(0.779083729));
    u_xlat16_4.x = fma(u_xlat16_19.y, Globals._GlossMapScale, (-u_xlat16_30));
    u_xlat16_4.x = u_xlat16_4.x + half(1.0);
    u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0h, 1.0h);
    u_xlat16_13.xyz = fma(Globals._Color.xyz, u_xlat16_2.xyz, half3(-0.220916301, -0.220916301, -0.220916301));
    u_xlat16_1.xyw = u_xlat16_2.xyz * Globals._Color.xyz;
    u_xlat16_7.xyz = half3(u_xlat16_30) * u_xlat16_1.xyw;
    u_xlat16_13.xyz = fma(u_xlat16_19.xxx, u_xlat16_13.xyz, half3(0.220916301, 0.220916301, 0.220916301));
    u_xlat16_8.xyz = (-u_xlat16_13.xyz) + u_xlat16_4.xxx;
    u_xlat16_8.xyz = fma(u_xlat16_3.yyy, u_xlat16_8.xyz, u_xlat16_13.xyz);
    u_xlat16_1.x = unity_NHxRoughness.sample(samplerunity_NHxRoughness, float2(u_xlat16_3.xz)).x;
    u_xlat16_1.x = u_xlat16_1.x * half(16.0);
    u_xlat16_3.xyz = fma(u_xlat16_1.xxx, u_xlat16_13.xyz, u_xlat16_7.xyz);
    u_xlat16_9.xyz = u_xlat16_9.xyz * u_xlat16_8.xyz;
    u_xlat16_1 = unity_DynamicLightmap.sample(samplerunity_DynamicLightmap, float2(input.TEXCOORD5.zw));
    u_xlat16_30 = u_xlat16_1.w * Globals.unity_DynamicLightmap_HDR.x;
    u_xlat16_4.xyz = u_xlat16_1.xyz * half3(u_xlat16_30);
    u_xlat16_4.xyz = log2(u_xlat16_4.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xyz * Globals.unity_DynamicLightmap_HDR.yyy;
    u_xlat16_4.xyz = exp2(u_xlat16_4.xyz);
    u_xlat16_1 = unity_DynamicDirectionality.sample(samplerunity_DynamicLightmap, float2(input.TEXCOORD5.zw));
    u_xlat16_8.xyz = u_xlat16_1.xyz + half3(-0.5, -0.5, -0.5);
    u_xlat16_1.x = max(u_xlat16_1.w, half(9.99999975e-05));
    u_xlat16_30 = dot(u_xlat16_5.xyz, u_xlat16_8.xyz);
    u_xlat16_31 = dot(u_xlat16_5.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_31 = clamp(u_xlat16_31, 0.0h, 1.0h);
    u_xlat16_30 = u_xlat16_30 + half(0.5);
    u_xlat16_4.xyz = half3(u_xlat16_30) * u_xlat16_4.xyz;
    u_xlat16_1.xyz = u_xlat16_4.xyz / u_xlat16_1.xxx;
    u_xlat16_4.xyz = u_xlat16_0.xxx * u_xlat16_1.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_4.xyz, u_xlat16_7.xyz, u_xlat16_9.xyz);
    u_xlat1.xyz = input.TEXCOORD8.xyz + (-Globals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = sqrt(u_xlat1.x);
    u_xlat10.xyz = (-input.TEXCOORD8.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat2.x = Globals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = Globals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = Globals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat10.x = dot(u_xlat10.xyz, u_xlat2.xyz);
    u_xlat1.x = (-u_xlat10.x) + u_xlat1.x;
    u_xlat1.x = fma(Globals.unity_ShadowFadeCenterAndType.w, u_xlat1.x, u_xlat10.x);
    u_xlat1.x = fma(u_xlat1.x, float(Globals._LightShadowData.z), float(Globals._LightShadowData.w));
    u_xlat1.x = clamp(u_xlat1.x, 0.0f, 1.0f);
    u_xlat10.xyz = input.TEXCOORD8.yyy * Globals.hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat10.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToShadow[0].xyz, input.TEXCOORD8.xxx, u_xlat10.xyz);
    u_xlat10.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToShadow[2].xyz, input.TEXCOORD8.zzz, u_xlat10.xyz);
    u_xlat10.xyz = u_xlat10.xyz + Globals.hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    u_xlat16_10 = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat10.xy, saturate(u_xlat10.z), level(0.0));
    u_xlat16_27 = (-Globals._LightShadowData.x) + half(1.0);
    u_xlat16_27 = fma(u_xlat16_10, u_xlat16_27, Globals._LightShadowData.x);
    u_xlat16_27 = half(u_xlat1.x + float(u_xlat16_27));
    u_xlat16_27 = clamp(u_xlat16_27, 0.0h, 1.0h);
    u_xlat16_4.xyz = half3(u_xlat16_27) * Globals._LightColor0.xyz;
    u_xlat16_4.xyz = half3(u_xlat16_31) * u_xlat16_4.xyz;
    output.SV_Target0.xyz = fma(u_xlat16_3.xyz, u_xlat16_4.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = half(1.0);
    return output;
}
                          Globals �        _WorldSpaceCameraPos                         _WorldSpaceLightPos0                       _LightShadowData                       unity_ShadowFadeCenterAndType                     0     unity_SpecCube0_HDR                  �     unity_DynamicLightmap_HDR                    �     _LightColor0                 �     _Color                   �     _GlossMapScale                   �     _OcclusionStrength                   �  	   _Parallax                    �     unity_WorldToShadow                        unity_MatrixV                    @  
      _ParallaxMap             _MainTex            _MetallicGlossMap               _OcclusionMap               unity_NHxRoughness              unity_DynamicLightmap                unity_DynamicDirectionality        ���   unity_SpecCube0             _ShadowMapTexture               Globals            