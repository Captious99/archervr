2O                         POINT      SHADOWS_CUBE   SHADOWS_SOFT   _METALLICGLOSSMAP      _PARALLAXMAP�     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _LightPositionRange;
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 _LightColor0;
    half4 _Color;
    half _GlossMapScale;
    half _Parallax;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    half3 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    sampler samplerunity_NHxRoughness [[ sampler (0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    sampler sampler_MetallicGlossMap [[ sampler (3) ]],
    sampler sampler_ParallaxMap [[ sampler (4) ]],
    sampler sampler_LightTexture0 [[ sampler (5) ]],
    texture2d<half, access::sample > _ParallaxMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (1) ]] ,
    texture2d<half, access::sample > _MetallicGlossMap [[ texture (2) ]] ,
    texturecube<float, access::sample > _ShadowMapTexture [[ texture (3) ]] ,
    texture2d<half, access::sample > _LightTexture0 [[ texture (4) ]] ,
    texture2d<half, access::sample > unity_NHxRoughness [[ texture (5) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half3 u_xlat16_0;
    float3 u_xlat1;
    half u_xlat16_1;
    float3 u_xlat2;
    half3 u_xlat16_2;
    half3 u_xlat16_3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    float4 u_xlat6;
    bool4 u_xlatb6;
    float3 u_xlat7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    half2 u_xlat16_10;
    float3 u_xlat12;
    bool u_xlatb12;
    half u_xlat16_14;
    half2 u_xlat16_21;
    half u_xlat16_30;
    float u_xlat33;
    half u_xlat16_34;
    u_xlat16_0.x = dot(input.TEXCOORD8.xyz, input.TEXCOORD8.xyz);
    u_xlat16_0.x = rsqrt(u_xlat16_0.x);
    u_xlat16_10.xy = u_xlat16_0.xx * input.TEXCOORD8.xy;
    u_xlat16_1 = _ParallaxMap.sample(sampler_ParallaxMap, input.TEXCOORD0.xy).y;
    u_xlat16_30 = Globals._Parallax * half(0.5);
    u_xlat16_30 = fma(u_xlat16_1, Globals._Parallax, (-u_xlat16_30));
    u_xlat16_0.x = fma(input.TEXCOORD8.z, u_xlat16_0.x, half(0.419999987));
    u_xlat16_0.xy = u_xlat16_10.xy / u_xlat16_0.xx;
    u_xlat1.xy = fma(float2(u_xlat16_30), float2(u_xlat16_0.xy), input.TEXCOORD0.xy);
    u_xlat16_21.xy = _MetallicGlossMap.sample(sampler_MetallicGlossMap, u_xlat1.xy).xw;
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, u_xlat1.xy).xyz;
    u_xlat16_3.xyz = u_xlat16_2.xyz * Globals._Color.xyz;
    u_xlat16_0.xyz = fma(Globals._Color.xyz, u_xlat16_2.xyz, half3(-0.220916301, -0.220916301, -0.220916301));
    u_xlat16_0.xyz = fma(u_xlat16_21.xxx, u_xlat16_0.xyz, half3(0.220916301, 0.220916301, 0.220916301));
    u_xlat16_30 = fma((-u_xlat16_21.x), half(0.779083729), half(0.779083729));
    u_xlat16_4.x = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat16_4.x = rsqrt(u_xlat16_4.x);
    u_xlat16_4.xyz = u_xlat16_4.xxx * input.TEXCOORD4.xyz;
    u_xlat16_34 = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_34 = rsqrt(u_xlat16_34);
    u_xlat16_5.xyz = half3(u_xlat16_34) * input.TEXCOORD1.xyz;
    u_xlat1.xyz = input.TEXCOORD5.yyy * Globals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD5.xxx, u_xlat1.xyz);
    u_xlat1.xyz = fma(Globals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD5.zzz, u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz + Globals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat2.xyz = (-input.TEXCOORD5.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat6.x = Globals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat6.y = Globals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat6.z = Globals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat6.xyz);
    u_xlat12.xyz = input.TEXCOORD5.xyz + (-Globals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat12.x = dot(u_xlat12.xyz, u_xlat12.xyz);
    u_xlat12.x = sqrt(u_xlat12.x);
    u_xlat12.x = (-u_xlat2.x) + u_xlat12.x;
    u_xlat2.x = fma(Globals.unity_ShadowFadeCenterAndType.w, u_xlat12.x, u_xlat2.x);
    u_xlat2.x = fma(u_xlat2.x, float(Globals._LightShadowData.z), float(Globals._LightShadowData.w));
    u_xlat2.x = clamp(u_xlat2.x, 0.0f, 1.0f);
    u_xlatb12 = u_xlat2.x<0.99000001;
    if(u_xlatb12){
        u_xlat12.xyz = input.TEXCOORD5.xyz + (-Globals._LightPositionRange.xyz);
        u_xlat33 = dot(u_xlat12.xyz, u_xlat12.xyz);
        u_xlat33 = sqrt(u_xlat33);
        u_xlat33 = u_xlat33 * Globals._LightPositionRange.w;
        u_xlat33 = u_xlat33 * 0.970000029;
        u_xlat6.xyz = u_xlat12.xyz + float3(0.0078125, 0.0078125, 0.0078125);
        u_xlat6.x = _ShadowMapTexture.sample(sampler_ShadowMapTexture, u_xlat6.xyz, level(0.0)).x;
        u_xlat7.xyz = u_xlat12.xyz + float3(-0.0078125, -0.0078125, 0.0078125);
        u_xlat6.y = _ShadowMapTexture.sample(sampler_ShadowMapTexture, u_xlat7.xyz, level(0.0)).x;
        u_xlat7.xyz = u_xlat12.xyz + float3(-0.0078125, 0.0078125, -0.0078125);
        u_xlat6.z = _ShadowMapTexture.sample(sampler_ShadowMapTexture, u_xlat7.xyz, level(0.0)).x;
        u_xlat12.xyz = u_xlat12.xyz + float3(0.0078125, -0.0078125, -0.0078125);
        u_xlat6.w = _ShadowMapTexture.sample(sampler_ShadowMapTexture, u_xlat12.xyz, level(0.0)).x;
        u_xlatb6 = (u_xlat6<float4(u_xlat33));
        u_xlat6.x = (u_xlatb6.x) ? float(Globals._LightShadowData.x) : float(1.0);
        u_xlat6.y = (u_xlatb6.y) ? float(Globals._LightShadowData.x) : float(1.0);
        u_xlat6.z = (u_xlatb6.z) ? float(Globals._LightShadowData.x) : float(1.0);
        u_xlat6.w = (u_xlatb6.w) ? float(Globals._LightShadowData.x) : float(1.0);
        u_xlat16_34 = half(dot(u_xlat6, float4(0.25, 0.25, 0.25, 0.25)));
    } else {
        u_xlat16_34 = half(1.0);
    }
    u_xlat16_34 = half(u_xlat2.x + float(u_xlat16_34));
    u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16_1 = _LightTexture0.sample(sampler_LightTexture0, u_xlat1.xx).x;
    u_xlat16_1 = u_xlat16_34 * u_xlat16_1;
    u_xlat16_8.x = input.TEXCOORD2.w;
    u_xlat16_8.y = input.TEXCOORD3.w;
    u_xlat16_8.z = input.TEXCOORD4.w;
    u_xlat16_34 = dot(u_xlat16_8.xyz, u_xlat16_8.xyz);
    u_xlat16_34 = rsqrt(u_xlat16_34);
    u_xlat16_8.xyz = half3(u_xlat16_34) * u_xlat16_8.xyz;
    u_xlat16_9.xyz = half3(u_xlat16_1) * Globals._LightColor0.xyz;
    u_xlat16_34 = dot((-u_xlat16_5.xyz), u_xlat16_4.xyz);
    u_xlat16_34 = u_xlat16_34 + u_xlat16_34;
    u_xlat16_5.xyz = fma(u_xlat16_4.xyz, (-half3(u_xlat16_34)), (-u_xlat16_5.xyz));
    u_xlat16_4.x = dot(u_xlat16_4.xyz, u_xlat16_8.xyz);
    u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0h, 1.0h);
    u_xlat16_14 = dot(u_xlat16_5.xyz, u_xlat16_8.xyz);
    u_xlat16_14 = u_xlat16_14 * u_xlat16_14;
    u_xlat16_5.x = u_xlat16_14 * u_xlat16_14;
    u_xlat16_5.y = fma((-u_xlat16_21.y), Globals._GlossMapScale, half(1.0));
    u_xlat16_1 = unity_NHxRoughness.sample(samplerunity_NHxRoughness, float2(u_xlat16_5.xy)).x;
    u_xlat16_1 = u_xlat16_1 * half(16.0);
    u_xlat16_0.xyz = u_xlat16_0.xyz * half3(u_xlat16_1);
    u_xlat16_0.xyz = fma(u_xlat16_3.xyz, half3(u_xlat16_30), u_xlat16_0.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xxx * u_xlat16_9.xyz;
    output.SV_Target0.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    output.SV_Target0.w = half(1.0);
    return output;
}
                             Globals �   
      _WorldSpaceCameraPos                         _LightPositionRange                         _LightShadowData                        unity_ShadowFadeCenterAndType                     0      _LightColor0                 �      _Color                   �      _GlossMapScale                   �   	   _Parallax                    �      unity_MatrixV                    @      unity_WorldToLight                   �         _ParallaxMap             _MainTex            _MetallicGlossMap               _ShadowMapTexture               _LightTexture0              unity_NHxRoughness               Globals            