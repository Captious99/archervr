2O                         DIRECTIONAL 
   _NORMALMAP     _METALLICGLOSSMAP      _PARALLAXMAP!     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct Globals_Type
{
    half4 _WorldSpaceLightPos0;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SpecCube0_HDR;
    half4 _LightColor0;
    half4 _Color;
    half _BumpScale;
    half _GlossMapScale;
    half _OcclusionStrength;
    half _Parallax;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_BumpMap [[ sampler (2) ]],
    sampler sampler_MetallicGlossMap [[ sampler (3) ]],
    sampler sampler_OcclusionMap [[ sampler (4) ]],
    sampler sampler_ParallaxMap [[ sampler (5) ]],
    texture2d<half, access::sample > _ParallaxMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (1) ]] ,
    texture2d<half, access::sample > _MetallicGlossMap [[ texture (2) ]] ,
    texture2d<half, access::sample > _BumpMap [[ texture (3) ]] ,
    texture2d<half, access::sample > _OcclusionMap [[ texture (4) ]] ,
    texturecube<half, access::sample > unity_SpecCube0 [[ texture (5) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half4 u_xlat16_0;
    float2 u_xlat1;
    half4 u_xlat16_1;
    half3 u_xlat16_2;
    half3 u_xlat16_3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    half3 u_xlat16_6;
    half3 u_xlat16_7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    half u_xlat16_11;
    half u_xlat16_20;
    half u_xlat16_21;
    half u_xlat16_30;
    half u_xlat16_33;
    half u_xlat16_35;
    half u_xlat16_36;
    u_xlat16_0.x = input.TEXCOORD2.w;
    u_xlat16_0.y = input.TEXCOORD3.w;
    u_xlat16_0.z = input.TEXCOORD4.w;
    u_xlat16_30 = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_30 = rsqrt(u_xlat16_30);
    u_xlat16_0.xy = half2(u_xlat16_30) * u_xlat16_0.xy;
    u_xlat16_20 = fma(u_xlat16_0.z, u_xlat16_30, half(0.419999987));
    u_xlat16_0.xy = u_xlat16_0.xy / half2(u_xlat16_20);
    u_xlat16_1.x = _ParallaxMap.sample(sampler_ParallaxMap, input.TEXCOORD0.xy).y;
    u_xlat16_20 = Globals._Parallax * half(0.5);
    u_xlat16_20 = fma(u_xlat16_1.x, Globals._Parallax, (-u_xlat16_20));
    u_xlat1.xy = fma(float2(u_xlat16_20), float2(u_xlat16_0.xy), input.TEXCOORD0.xy);
    u_xlat16_2.xyz = _BumpMap.sample(sampler_BumpMap, u_xlat1.xy).xyz;
    u_xlat16_0.xyz = fma(u_xlat16_2.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -1.0));
    u_xlat16_0.xy = u_xlat16_0.xy * half2(Globals._BumpScale);
    u_xlat16_3.xyz = u_xlat16_0.yyy * input.TEXCOORD3.xyz;
    u_xlat16_0.xyw = fma(input.TEXCOORD2.xyz, u_xlat16_0.xxx, u_xlat16_3.xyz);
    u_xlat16_0.xyz = fma(input.TEXCOORD4.xyz, u_xlat16_0.zzz, u_xlat16_0.xyw);
    u_xlat16_30 = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_30 = rsqrt(u_xlat16_30);
    u_xlat16_0.xyz = half3(u_xlat16_30) * u_xlat16_0.xyz;
    u_xlat16_0.w = half(1.0);
    u_xlat16_3.x = dot(Globals.unity_SHAr, u_xlat16_0);
    u_xlat16_3.y = dot(Globals.unity_SHAg, u_xlat16_0);
    u_xlat16_3.z = dot(Globals.unity_SHAb, u_xlat16_0);
    u_xlat16_3.xyz = u_xlat16_3.xyz + input.TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_2.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_2.xyz = u_xlat16_2.xyz * half3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_2.xyz = exp2(u_xlat16_2.xyz);
    u_xlat16_2.xyz = fma(u_xlat16_2.xyz, half3(1.05499995, 1.05499995, 1.05499995), half3(-0.0549999997, -0.0549999997, -0.0549999997));
    u_xlat16_2.xyz = max(u_xlat16_2.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_21 = _OcclusionMap.sample(sampler_OcclusionMap, u_xlat1.xy).y;
    u_xlat16_30 = (-Globals._OcclusionStrength) + half(1.0);
    u_xlat16_30 = fma(u_xlat16_21, Globals._OcclusionStrength, u_xlat16_30);
    u_xlat16_3.xyz = half3(u_xlat16_30) * u_xlat16_2.xyz;
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, u_xlat1.xy).xyz;
    u_xlat16_1.xy = _MetallicGlossMap.sample(sampler_MetallicGlossMap, u_xlat1.xy).xw;
    u_xlat16_4.xyz = u_xlat16_2.xyz * Globals._Color.xyz;
    u_xlat16_5.xyz = fma(Globals._Color.xyz, u_xlat16_2.xyz, half3(-0.220916301, -0.220916301, -0.220916301));
    u_xlat16_5.xyz = fma(u_xlat16_1.xxx, u_xlat16_5.xyz, half3(0.220916301, 0.220916301, 0.220916301));
    u_xlat16_33 = fma((-u_xlat16_1.x), half(0.779083729), half(0.779083729));
    u_xlat16_6.xyz = half3(u_xlat16_33) * u_xlat16_4.xyz;
    u_xlat16_33 = fma(u_xlat16_1.y, Globals._GlossMapScale, (-u_xlat16_33));
    u_xlat16_35 = fma((-u_xlat16_1.y), Globals._GlossMapScale, half(1.0));
    u_xlat16_33 = u_xlat16_33 + half(1.0);
    u_xlat16_33 = clamp(u_xlat16_33, 0.0h, 1.0h);
    u_xlat16_7.xyz = (-u_xlat16_5.xyz) + half3(u_xlat16_33);
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_6.xyz;
    u_xlat16_1.x = fma(u_xlat16_35, u_xlat16_35, half(1.5));
    u_xlat16_33 = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_33 = rsqrt(u_xlat16_33);
    u_xlat16_8.xyz = fma((-input.TEXCOORD1.xyz), half3(u_xlat16_33), Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_9.xyz = half3(u_xlat16_33) * input.TEXCOORD1.xyz;
    u_xlat16_33 = dot(u_xlat16_8.xyz, u_xlat16_8.xyz);
    u_xlat16_11 = max(u_xlat16_33, half(0.00100000005));
    u_xlat16_33 = rsqrt(u_xlat16_11);
    u_xlat16_8.xyz = half3(u_xlat16_33) * u_xlat16_8.xyz;
    u_xlat16_33 = dot(Globals._WorldSpaceLightPos0.xyz, u_xlat16_8.xyz);
    u_xlat16_33 = clamp(u_xlat16_33, 0.0h, 1.0h);
    u_xlat16_36 = dot(u_xlat16_0.xyz, u_xlat16_8.xyz);
    u_xlat16_36 = clamp(u_xlat16_36, 0.0h, 1.0h);
    u_xlat16_36 = u_xlat16_36 * u_xlat16_36;
    u_xlat16_11 = max(u_xlat16_33, half(0.319999993));
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_11;
    u_xlat16_33 = u_xlat16_35 * u_xlat16_35;
    u_xlat16_11 = fma(u_xlat16_33, u_xlat16_33, half(-1.0));
    u_xlat16_11 = fma(u_xlat16_36, u_xlat16_11, half(1.00001001));
    u_xlat16_1.x = u_xlat16_11 * u_xlat16_1.x;
    u_xlat16_1.x = u_xlat16_33 / u_xlat16_1.x;
    u_xlat16_33 = u_xlat16_35 * u_xlat16_33;
    u_xlat16_33 = fma((-u_xlat16_33), half(0.280000001), half(1.0));
    u_xlat16_1.x = u_xlat16_1.x + half(-9.99999975e-05);
    u_xlat16_36 = max(u_xlat16_1.x, half(0.0));
    u_xlat16_36 = min(u_xlat16_36, half(100.0));
    u_xlat16_6.xyz = fma(half3(u_xlat16_36), u_xlat16_5.xyz, u_xlat16_6.xyz);
    u_xlat16_6.xyz = u_xlat16_6.xyz * Globals._LightColor0.xyz;
    u_xlat16_36 = dot(u_xlat16_0.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_36 = clamp(u_xlat16_36, 0.0h, 1.0h);
    u_xlat16_3.xyz = fma(u_xlat16_6.xyz, half3(u_xlat16_36), u_xlat16_3.xyz);
    u_xlat16_6.x = fma((-u_xlat16_35), half(0.699999988), half(1.70000005));
    u_xlat16_35 = u_xlat16_35 * u_xlat16_6.x;
    u_xlat16_35 = u_xlat16_35 * half(6.0);
    u_xlat16_6.x = dot(u_xlat16_9.xyz, u_xlat16_0.xyz);
    u_xlat16_6.x = u_xlat16_6.x + u_xlat16_6.x;
    u_xlat16_6.xyz = fma(u_xlat16_0.xyz, (-u_xlat16_6.xxx), u_xlat16_9.xyz);
    u_xlat16_0.x = dot(u_xlat16_0.xyz, (-u_xlat16_9.xyz));
    u_xlat16_0.x = clamp(u_xlat16_0.x, 0.0h, 1.0h);
    u_xlat16_0.x = (-u_xlat16_0.x) + half(1.0);
    u_xlat16_0.x = u_xlat16_0.x * u_xlat16_0.x;
    u_xlat16_0.x = u_xlat16_0.x * u_xlat16_0.x;
    u_xlat16_0.xyz = fma(u_xlat16_0.xxx, u_xlat16_7.xyz, u_xlat16_5.xyz);
    u_xlat16_1 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_6.xyz), level(float(u_xlat16_35)));
    u_xlat16_5.x = u_xlat16_1.w + half(-1.0);
    u_xlat16_5.x = fma(Globals.unity_SpecCube0_HDR.w, u_xlat16_5.x, half(1.0));
    u_xlat16_5.x = u_xlat16_5.x * Globals.unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat16_1.xyz * u_xlat16_5.xxx;
    u_xlat16_5.xyz = half3(u_xlat16_30) * u_xlat16_5.xyz;
    u_xlat16_5.xyz = half3(u_xlat16_33) * u_xlat16_5.xyz;
    output.SV_Target0.xyz = fma(u_xlat16_5.xyz, u_xlat16_0.xyz, u_xlat16_3.xyz);
    output.SV_Target0.w = half(1.0);
    return output;
}
                          Globals @         _WorldSpaceLightPos0                     
   unity_SHAr                      
   unity_SHAg                      
   unity_SHAb                         unity_SpecCube0_HDR                         _LightColor0                 (      _Color                   0   
   _BumpScale                   8      _GlossMapScale                   :      _OcclusionStrength                   <   	   _Parallax                    >         _ParallaxMap             _MainTex            _MetallicGlossMap               _BumpMap            _OcclusionMap               unity_SpecCube0              Globals            